#!/usr/bin/python3

import random
import csv


# lecture du fichier csv
def lire_questions(fichier):
    with open(fichier, newline='', encoding='utf-8') as csvfile:
        lecteur = csv.reader(csvfile)
        questions = list(lecteur)
    return questions


tabQuestion = lire_questions("QR.csv")

print(tabQuestion)
